from email.policy import default
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

# Create your models here.

class Book(models.Model):
    isbn = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    description = models.TextField()
    category_id = models.IntegerField()
    author_id = models.IntegerField()
    location_id = models.IntegerField()
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    image = models.FileField()
    created = models.DateTimeField(default=timezone.now)
    modified = models.DateTimeField(default=timezone.now)



